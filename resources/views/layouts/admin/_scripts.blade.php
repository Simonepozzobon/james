<script src="//code.jquery.com/jquery-3.1.1.min.js" crossorigin="anonymous"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" crossorigin="anonymous"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" crossorigin="anonymous"></script>

{{-- Bootstrap --}}
<script src="{{ asset('admin-assets/js/image-picker.min.js') }}"></script>
<script src="{{ asset('admin-assets/js/custom.js') }}"></script>

@yield('scripts')
