<?php

use App\Post;
use App\Partner;

// use Symfony\Component\Debug\Debug;
// Debug::enable();

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
|--------------------------------------------------------------------------
| Public Routes
|--------------------------------------------------------------------------
*/


/*
|--------------------------------------------------------------------------
| WORKING
|--------------------------------------------------------------------------
*/

Route::get('video-test', 'Admin\VideoController@index')->name('video-test.index');


/*
|--------------------------------------------------------------------------
| NEED UPDATE
|--------------------------------------------------------------------------
*/

// need to create a controller for these maybe Main or FrontendController
Route::get('/', function () {
    return view('new');
});

/*
|--------------------------------------------------------------------------
| DONE
|--------------------------------------------------------------------------
*/

// Auth
Auth::routes();
// Logout
Route::get('/logout', 'Auth\LoginController@logout');


/*
|
|
|--------------------------------------------------------------------------
| Admin Routes (Admin Panel)
|--------------------------------------------------------------------------
|
*/

// Admin Panel Routes
Route::prefix('admin')->group(function () {

  /*
  |--------------------------------------------------------------------------
  | WORKING
  |--------------------------------------------------------------------------
  | - add video library to manage all the video in once then assign to apps.
  */
  Route::prefix('/video-library')->group(function() {
    Route::resource('/video-api-library', 'Admin\VideoLibraryController');
    Route::get('/', function() {
      return view('admin.video_library.index');
    })->name('video-library.index');
  });

  /*
  |--------------------------------------------------------------------------
  | DONE
  |--------------------------------------------------------------------------
  */
  // Auth
  Route::get('/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
  Route::post('/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');
  Route::get('/logout', 'Auth\AdminLoginController@logout')->name('admin.logout');
  Route::get('/', 'AdminController@index')->name('admin');

  // Users menu routes
  Route::get('/admins', 'Admin\AdminController@index')->name('admin.admins.index');
  Route::resource('teachers', 'Admin\TeacherController', ['except' => ['create']]);
  Route::post('teachers/{teacher}', 'Admin\TeacherController@storeStudent')->name('teacher.store.student');
  Route::resource('students', 'Admin\StudentController', ['except' => ['create']]);
  Route::resource('users', 'Admin\UserController', ['except' => ['create']]);
  Route::resource('schools', 'Admin\SchoolController', ['except' => ['create']]);

  // Settings menu routes
  Route::resource('partners', 'Admin\PartnerController', ['except' => ['create'] ]);

});


/*
|
|
|--------------------------------------------------------------------------
| Teacher Routes (Teacher Panel)
|--------------------------------------------------------------------------
|
*/

// Teacher Panel Teacher
Route::prefix('teacher')->group(function() {
  // Auth
  Route::get('/login', 'Auth\TeacherLoginController@showLoginForm')->name('teacher.login');
  Route::post('/login', 'Auth\TeacherLoginController@login')->name('teacher.login.submit');
  Route::get('/logout', 'Auth\TeacherLoginController@logout')->name('teacher.logout');
  Route::get('/', 'TeacherController@index')->name('teacher');

  // Apps
  Route::get('/app', 'AppController@firstApp')->name('app.first');
});


/*
|
|
|--------------------------------------------------------------------------
| Student Routes (Student Panel)
|--------------------------------------------------------------------------
|
*/

// Student Panel Routes
Route::prefix('student')->group(function() {
  // Auth
  Route::get('/login', 'Auth\StudentLoginController@showLoginForm')->name('student.login');
  Route::post('/login', 'Auth\StudentLoginController@login')->name('student.login.submit');
  Route::get('/logout', 'Auth\StudentLoginController@logout');
  Route::get('/', 'StudentController@index')->name('student');
});
